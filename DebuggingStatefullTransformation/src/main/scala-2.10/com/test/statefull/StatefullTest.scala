package com.test.statefull

import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import com.datastax.spark.connector._
import com.datastax.spark.connector.streaming.toDStreamFunctions


/**
  * Created by srdjan on 7/06/16.
  */
object StatefullTest {

  def main(args: Array[String]) = {

    val conf = new SparkConf()
      .setAppName("udtTest")
      .set("spark.cassandra.connection.host","localhost")

    val ssc = new StreamingContext(conf,Seconds(5))

    val stream = ssc.socketTextStream("localhost", 9091)

    val updatedStream = stream.map(a => {
      val b = a.split(",")
      val id = b(0).toInt
      val value = b(1).toInt
      id->Input(value)
    })

    val toWrite = updatedStream.updateStateByKey(updateStateByKeyFunction)
      .map( a => {
        (a._1,a._2.difference)
      })

    toWrite.saveToCassandra("test_keyspace","absolute_difference", SomeColumns("id","difference"))

    ssc.checkpoint("/home/vagrant/debuggingStatefulTransformation_Checkpoint")
    ssc.start()
    ssc.awaitTermination()

  }

  def updateStateByKeyFunction(incomingData: Seq[Input], state: Option[State]):Option[State] = {
    var result:Option[State] = state

    if (incomingData.isEmpty){
        result = state
    }
    else
      {
        val rawData = incomingData.last
        if (state.isEmpty){
          result = Some(State(rawData.value, 0))
        }
        else {
          if (rawData.value>state.get.previousValue)
            result = Some(State(rawData.value, rawData.value-state.get.previousValue))
          else {
            result = Some(State(rawData.value, state.get.previousValue - rawData.value))
          }
        }
      }
    result
  }

}

case class Input (
                 value: Int
                 )

case class State (
                 previousValue: Int,
                 difference: Int
                 )
