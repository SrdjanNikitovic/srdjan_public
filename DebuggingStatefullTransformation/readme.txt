Create keyspace:

CREATE KEYSPACE test_keyspace WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor' : 1 };


CREATE TABLE absolute_difference (
 id int,
 difference int,
 primary key (id)
 );


Opet socket text stream before running the job:

nc -l -p 9091


Running job:

/usr/share/spark/bin/spark-submit --master spark://192.168.33.30:7077 \
--packages com.datastax.spark:spark-cassandra-connector_2.10:1.4.0 DebuggingStatefullTransformation-assembly-0.1-SNAPSHOT.jar

