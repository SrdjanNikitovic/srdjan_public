import Dependencies._
import com.typesafe.sbt.packager.NativePackagerKeys
import com.typesafe.sbt.packager.archetypes.JavaAppKeys
import com.typesafe.sbt.packager.linux.LinuxKeys
import sbt.Keys._
import sbt._
import sbtassembly.AssemblyKeys._
import sbtassembly.{MergeStrategy, PathList}
import sbtassembly.AssemblyPlugin.assemblySettings

object BuildSettings extends NativePackagerKeys with JavaAppKeys with LinuxKeys {
  val SCALA_2_10 = "2.10.6"
  val SCALA_2_11 = "2.11.7"

  lazy val sparkSettings = Seq(
    scalaVersion := SCALA_2_10
  )

  lazy val commonSettings = Seq(
    version := "0.2.0"
    , (assemblyMergeStrategy in assembly) := {
      case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
      case PathList("META-INF", "LICENSE") => MergeStrategy.discard
      case PathList("META-INF", "LICENSE.txt") => MergeStrategy.discard
      case PathList("META-INF", "NOTICE") => MergeStrategy.discard
      case PathList("META-INF", "NOTICE.txt") => MergeStrategy.discard
      case PathList("META-INF", "NOTICE.txt") => MergeStrategy.discard
      case PathList("META-INF", "DEPENDENCIES") => MergeStrategy.discard
      case PathList("META-INF", "INDEX.LIST") => MergeStrategy.discard
      case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.discard
      case PathList("META-INF", "maven","com.google.guava","guava","pom.properties") => MergeStrategy.discard
      case PathList("META-INF", "maven","com.google.guava","guava","pom.xml") => MergeStrategy.discard
      case _ => MergeStrategy.deduplicate
    }
  )
}

object MyBuild extends Build {

  import BuildSettings._

  val root = Project("root", file("."))
    .aggregate(DebuggingUDT)


  // Spark Pipeline that inserts into UDT Type
  lazy val DebuggingUDT : Project = (project in file("DebuggingUDT")).
    settings(assemblySettings: _*).
    settings(sparkSettings: _*).
    settings(commonSettings: _*).
    settings(
      artifact in (Compile, assembly) ~= { art =>
        art.copy(`classifier` = Some("assembly"))
      }
    ).
    settings(
      libraryDependencies ++= (commonDependencies ++ pipelineDependencies)
    )

  // Spark Pipeline that tests statefull transformation
  lazy val DebuggingStatefullTransformation : Project = (project in file("DebuggingStatefullTransformation")).
    settings(assemblySettings: _*).
    settings(sparkSettings: _*).
    settings(commonSettings: _*).
    settings(
      artifact in (Compile, assembly) ~= { art =>
        art.copy(`classifier` = Some("assembly"))
      }
    ).
    settings(
      libraryDependencies ++= (commonDependencies ++ pipelineDependencies)
    )

}

