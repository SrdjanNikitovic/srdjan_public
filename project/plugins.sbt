addSbtPlugin("com.typesafe.sbt" % "sbt-scalariform" % "1.3.0")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.1")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.1.0")

resolvers += "SBT plugins" at "https://dl.bintray.com/sbt/sbt-plugin-releases/"

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.0-M3")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.2")

libraryDependencies += "org.vafer" % "jdeb" % "1.3" artifacts (Artifact("jdeb", "jar", "jar"))

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")



