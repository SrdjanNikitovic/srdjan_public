import sbt._

object Version {
  val spark     = "1.4.0"
  val scalatest = "2.2.6"
  //val sparkCassandraConnector = "1.4.4"
  val sparkCassandraConnector = "1.4.3"
}

object Dependencies {

  val commonDependencies = Seq(
    "org.scalatest"    %% "scalatest"       % "2.2.4"       % "test"
  )

    val pipelineDependencies = Seq (
      "org.apache.spark"                          %% "spark-core"                % Version.spark % "provided"
      , "org.apache.spark"                        %% "spark-streaming"           % Version.spark % "provided"
      , "org.apache.spark"                        %% "spark-sql"                 % Version.spark % "provided"
      , "org.apache.spark"                        %% "spark-streaming-kafka"     % Version.spark
             exclude("org.spark-project.spark","unused")
      ,"com.datastax.spark"                      %% "spark-cassandra-connector" % Version.sparkCassandraConnector
             exclude("org.spark-project.spark","unused")
    )


  val debuggingUDTDependencies = commonDependencies ++ pipelineDependencies
}
