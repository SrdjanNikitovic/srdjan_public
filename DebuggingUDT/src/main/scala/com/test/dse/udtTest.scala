package com.test.dse

import com.datastax.spark.connector._
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by srdjan on 7/06/16.
  */

case class Name (
                  first_name: String,
                  last_name: String
                )

case class Actor(
                  id: Int,
                  name: Name
                )

object udtTest {

  def main(args: Array[String]) = {

    val conf = new SparkConf()
      .setAppName("udtTest")
      .set("spark.cassandra.connection.host","172.31.36.3")

    val sc = new SparkContext(conf)

    val name1 = Name("Brus", "Willis")
    val name2 = Name("Jessica", "Alba")

    val actor1 = Actor(1, name1)
    val actor2 = Actor(2, name2)

    val rdd = sc.parallelize(Seq(actor1, actor2))

    rdd.saveToCassandra("test_keyspace","actors")

  }
}

