Create keyspace:

CREATE KEYSPACE test_keyspace WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor' : 1 };


CREATE TYPE Name (
 first_name varchar,
 last_name varchar
);


CREATE TABLE actors (
 id int,
 name frozen<Name>,
 primary key (id)
 );

 In the udtTest.scala, it is necessary to set "spark.cassandra.connection.host"

Running job:

~/spark-submit --master master_ip_address:port\
 --packages com.datastax.spark:spark-cassandra-connector_2.10:1.4.4 DebuggingUDT-assembly-0.1-SNAPSHOT.jar
